import { showModal } from './modal';

export function showWinnerModal(fighter) {
  let winner = { title: fighter.name, bodyElement: `This time ${fighter.name} got the lead!` };
  // call showModal function 
  showModal(winner);
}
