import { createElement } from '../helpers/domHelper';
import { fighters, fightersDetails } from '../helpers/mockData';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const imageElement = createFighterImage(fighter);
  fighterElement.append(imageElement);

  for (const [key, value] of Object.entries(fighter)) { //for every poperty in fighter object making own <span> element
    if (key == '_id' || key == 'source') { // passing id and sourse proprty
      continue;
    } else if (key == 'name') {
      const descriptionLine = createElement({ //creating elemet for every property
        tagName: 'span',
        className: 'fighter-preview__description-line-name',
      })
      descriptionLine.append(`${value}`);
      fighterElement.append(descriptionLine);
    } else {
      const descriptionLine = createElement({ //creating elemet for every property
        tagName: 'span',
        className: 'fighter-preview__description-line',
      })
      descriptionLine.append(`${key}: ${value}`); // appending text to element
      fighterElement.append(descriptionLine); // appending element to fighterElement
    }

  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createDescriptionElement(fighter) {

}


