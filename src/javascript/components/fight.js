import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {



  return new Promise((resolve) => {
    let firstHealthPoints = firstFighter.health; // healthpoints of fighter
    let secondHealthPoints = secondFighter.health; // healthpoints of fighter

    let firstBlock = false; // flag that shows is the block active
    let secondBlock = false; // flag that shows is the block active

    let firstHealthIndicator = document.getElementById("left-fighter-indicator"); // html elemenent that shows health indicator
    let secondHealthIndicator = document.getElementById("right-fighter-indicator"); // html elemenent that shows health indicator

    let PlayerOneCriticalFlags = { KeyQ: false, KeyW: false, KeyE: false }; // Object with flags for combinations
    let PlayerTwoCriticalFlags = { KeyU: false, KeyI: false, KeyO: false }; // Object with flags for combinations

    let firstPlayerTimeOut = true; // TimeOut flag for combos
    let secondPlayerTimeOut = true; // TimeOut flag for combo

    const createTimeoutPromise = (message, ms) => // Functions that create time out promice for combos cool down
      new Promise(resolve => {
        setTimeout(() => resolve(message), ms);
      });

    function changeHealthbar(currentHealthPoints, generalHealthPoints) { // Functions that changes healthbar after attack
      return currentHealthPoints / generalHealthPoints * 100 + '%'
    }

    function promiceResolve() { // Function that resolves promice with the winner after fight
      if (firstHealthPoints <= 0 || secondHealthPoints <= 0) {
        resolve(firstHealthPoints >= 0 ? firstFighter : secondFighter);
      }
    }

    document.addEventListener('keyup', (event) => { // setting hits on keyup for on click = one hit
      let keyCode = event.code;
      if (keyCode == controls.PlayerOneAttack && firstBlock == false) { // Hit for player one
        secondHealthPoints -= secondBlock ? 0 : getDamage(firstFighter, secondFighter); // reducing health after hit
        secondHealthIndicator.style.width = changeHealthbar(secondHealthPoints, secondFighter.health); //recalculating health bar
        promiceResolve();
      }
      else if (keyCode == controls.PlayerTwoAttack && secondBlock == false) {
        firstHealthPoints -= firstBlock ? 0 : getDamage(secondFighter, firstFighter);
        firstHealthIndicator.style.width = changeHealthbar(firstHealthPoints, firstFighter.health);
        promiceResolve();
      }
      else if (keyCode == controls.PlayerOneBlock) { // setting block flag to false when the key is released
        firstBlock = false;
      }
      else if (keyCode == controls.PlayerTwoBlock) {
        secondBlock = false;
      }
      else if (keyCode == controls.PlayerOneCriticalHitCombination[0] || keyCode == controls.PlayerOneCriticalHitCombination[1] || keyCode == controls.PlayerOneCriticalHitCombination[2]) { // checkup for combo flags
        PlayerOneCriticalFlags[keyCode] = false // setting combo key flag down

      }
      else if (keyCode == controls.PlayerTwoCriticalHitCombination[0] || keyCode == controls.PlayerTwoCriticalHitCombination[1] || keyCode == controls.PlayerTwoCriticalHitCombination[2]) {
        PlayerTwoCriticalFlags[keyCode] = false
      }
    });

    document.addEventListener('keydown', (event) => { // keydown to make proper blocks and combinations
      let keyCode = event.code;
      if (keyCode == controls.PlayerOneBlock) {
        firstBlock = true;
      }
      else if (keyCode == controls.PlayerTwoBlock) {
        secondBlock = true;
      }
      else if (keyCode == controls.PlayerOneCriticalHitCombination[0] || keyCode == controls.PlayerOneCriticalHitCombination[1] || keyCode == controls.PlayerOneCriticalHitCombination[2]) {
        PlayerOneCriticalFlags[keyCode] = true
        if (PlayerOneCriticalFlags.KeyQ && PlayerOneCriticalFlags.KeyW && PlayerOneCriticalFlags.KeyE && firstPlayerTimeOut) {
          secondHealthPoints -= firstFighter.attack * 2; // formulae for combo
          promiceResolve();
          secondHealthIndicator.style.width = changeHealthbar(secondHealthPoints, secondFighter.health); // recalculating heath bar
          firstPlayerTimeOut = false; // setting cool down combo flag
          const firstFighterTimeOutPromice = createTimeoutPromise('TimeOut end', 10000); // creating promice to change combo flag after 10 seconds
          firstFighterTimeOutPromice.then(res => firstPlayerTimeOut = true); // changing combo flag after 10 seconds
        }
      }
      else if (keyCode == controls.PlayerTwoCriticalHitCombination[0] || keyCode == controls.PlayerTwoCriticalHitCombination[1] || keyCode == controls.PlayerTwoCriticalHitCombination[2]) {
        PlayerTwoCriticalFlags[keyCode] = true
        if (PlayerTwoCriticalFlags.KeyU && PlayerTwoCriticalFlags.KeyI && PlayerTwoCriticalFlags.KeyO && secondPlayerTimeOut) {
          firstHealthPoints -= secondFighter.attack * 2;
          promiceResolve();
          firstHealthIndicator.style.width = firstHealthPoints / firstFighter.health * 100 + '%';
          secondPlayerTimeOut = false;
          const secondFighterTimeOutPromice = createTimeoutPromise('TimeOut end', 10000);
          secondFighterTimeOutPromice.then(res => secondPlayerTimeOut = true);
        }
      }
    });
  });
}

export function getDamage(attacker, defender) {
  let hit = getHitPower(attacker);
  let block = getBlockPower(defender);
  return block >= hit ? 0 : hit - block;
}

export function getHitPower(fighter) {
  let attack = fighter.attack;
  let criticalHitChance = Math.random() + 1;
  let power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  let defense = fighter.defense;
  let dodgeChance = Math.random() + 1;
  let power = defense * dodgeChance;
  return power;
}
